let createError = require('http-errors');
let express = require('express');
let bodyParser = require('body-parser');
let path = require('path');
let logger = require('morgan');
let jwt = require('jsonwebtoken');
let jwtSecret = require('./config/jwt_secret');

let authRouter = require('./routes/auth');
let receiverRouter = require('./routes/receiver');
let resultsRouter = require('./routes/results');
let notificationRouter = require('./routes/notification');

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", false);
    res.header("Access-Control-Allow-Headers", "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, authorization");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

    if (req.method === 'OPTIONS') {
        res.status(200).send({});
    } else {
        next();
    }
});

app.use(function(req, res, next) {

    console.log('---------- ' + req.method + ' : ' + req.url + ' ---------');
    console.log('req.headers=' + JSON.stringify(req.headers));
    console.log('req.query=' + JSON.stringify(req.query));
    console.log('req.body=' + JSON.stringify(req.body));

    let unauthUrls = [
        '/api/v1/auth/register-instance',
        '/api/v1/auth/ping',
        '/api/v1/auth/firebase-login'
    ];

    if (unauthUrls.indexOf(req.url) > -1) {
        next();
    } else {
        let token = req.headers['authorization'];
        if (token) {
            console.log('authorization=' + token);
            jwt.verify(token, jwtSecret.secret, function (err, decoded) {
                if (err) {
                    console.log('error=' + err);
                    res.status(403).send({result: 'Invalid Token'});
                    return;
                }

                if (decoded['exp'] < new Date().getTime()) {
                    res.status(403).send({result: 'Token expired'});
                    return;
                }
                res.req.headers['user-id'] = decoded['user-id'];
                res.req.headers['inst-id'] = decoded['inst-id'];
                next();
            });
        } else {
            res.status(401).json({result: 'Token not exists'});
            return;
        }
    }
});

app.use('/api/v1/auth', authRouter);
app.use('/api/v1/reception', receiverRouter);
app.use('/api/v1/results', resultsRouter);
app.use('/api/v1/notification', notificationRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;