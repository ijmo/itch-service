let express = require('express');
let router = express.Router();
let mysql = require('mysql');
let dbconfig = require('../config/mysql.js');
let connection = mysql.createConnection(dbconfig);
let admin = require('firebase-admin');
let serviceAccount = require('../config/itch-tector-firebase-adminsdk-fetyz-9c96315bfb');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://itch-tector.firebaseio.com'
});
let jwt = require('jsonwebtoken');
let jwtSecret = require('../config/jwt_secret');


router.post('/register-instance', function(req, res) {
    let instId = req.body.instId;
    let deviceModel = req.body.deviceModel;
    let platform = req.body.platform;

    connection.query('call REGISTER_APP_INSTANCE(?,?,?)', [instId, platform, deviceModel], function (err, rows) {
        if (err) throw err;
        console.log(rows[0][0]);
        res.json({'instanceId': rows[0][0].inst_id});
    });
});

router.post('/ping', function(req, res) {
    let instId = req.body.instId;
    let platform = req.body.platform;
    let userKey = req.body.userKey;
    let extra = req.body.extra;

    connection.query('call INSERT_PING(?,?,?)', [instId, userKey, extra], function(err, rows) {
        if (err) throw err;
        console.log(rows[0][0]);
    });
    res.json({'result': 'pong'});
});

function generateToken(userId, instId, ttl) {
    let expTime = new Date(new Date().getTime() + (1000 * ttl)).getTime();
    console.log("expTime=" + expTime);
    let token = jwt.sign({ 'user-id': userId, 'inst-id': instId, 'exp': expTime }, jwtSecret.secret);
    return token;
}

router.post('/firebase-login', function(req, res) {
    let instId = req.body.instId;
    let origin = req.body.origin;
    let platform = req.body.platform;
    let token = req.body.token;

    let provider = null;

    switch (origin) {
        case 'google':
            provider = 'G';
            break;
        case 'facebook':
            provider = 'F';
            break;
        case 'phone':
            provider = 'P';
            break;
        case 'password':
            provider = 'E';
            break;
    }

    admin.auth().verifyIdToken(token)
        .then(function(decodedToken) {
            console.log('decodedToken=' + JSON.stringify(decodedToken));
            console.log(decodedToken);

            let email = decodedToken.email;

            connection.query('call CREATE_USER_ACCOUNT(?,?,?,?)', [email, provider, '', ''], function (err, rows) {
                if (err) throw err;
                console.log(rows[0][0]);

                let userId = rows[0][0].user_id;
                let userKey = rows[0][0].user_key;

                let refreshToken = generateToken(userId, instId, 525600); // 525600 mins = 1 year
                let accessToken = generateToken(userId, instId, 720); // ;; 720 mins = 12 hours

                console.log('refreshToken=' + refreshToken);
                console.log('accessToken=' + accessToken);

                res.json({
                    data: {
                        refreshToken: refreshToken,
                        accessToken: accessToken,
                        userKey: userId.toString() // use userId as userKey for convenience in alpha test
                    }
                });
            });
        }).catch(function(error) {
        console.log('Error fetching verify id:', error);
        res.json({'result': 'failed'});
    });
});

router.post('/new-access-token', function(req, res) {
    let userId = req.headers['user-id'];
    let instId = req.headers['inst-id'];

    let accessToken = generateToken(userId, instId, 720); // ;; 720 mins = 12 hours

    res.json({
        data: {
            accessToken: accessToken,
        }
    });
});

module.exports = router;