let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');
let conn = mongoose.createConnection('mongodb://mongo/results', { useNewUrlParser: true });

conn.on('connected', function() {
    console.log('mongodb connected: results')
});
conn.on('disconnected', function() {
    console.log('mongodb disconnected')
});
let resultSchema = new mongoose.Schema({
    surveyId: String,
    userId: Number,
    instId: String,
    startTime: Number,
    endTime: Number,
    data: [[Number, Number, String, String, Number, Number]]
});
let Result = mongoose.model('Result', resultSchema, 'result');


router.get('/all-data', function(req, res) {
    let userId = req.headers['user-id'];
    let instId = req.headers['inst-id'];
    let surveyId = req.body.surveyId;

    Result.find({userId: userId}, { _id: 0, startTime: 1, endTime: 1, data: 1})
        .sort({startTime: -1})
        .exec(function(err, result) {
            if (err) return console.error(err);

            console.log("result=" + result);
            res.send(result);
        });
});

// router.options('/last', cors());
router.get('/last', function(req, res) {
    let userId = req.headers['user-id'];
    let instId = req.headers['inst-id'];
    let num = Number(req.query.n);

    Result.find({userId: userId}, { _id: 0, startTime: 1, endTime: 1, data: 1})
        .sort({startTime: -1})
        .limit(num)
        .exec(function(err, result) {
            if (err) return console.error(err);

            console.log("result=" + result);
            res.send(result);
        });
});

router.get('/reversed', function(req, res) {
    let userId = req.headers['user-id'];
    let instId = req.headers['inst-id'];
    let index = Number(req.query.i);

    Result.find({userId: userId}, { _id: 0, startTime: 1, endTime: 1, data: 1})
        .sort({startTime: -1})
        .skip(index)
        .limit(1)
        .exec(function(err, result) {
            if (err) return console.error(err);

            console.log("result=" + result);
            res.send(result);
        });
});


module.exports = router;