let express = require('express');
let router = express.Router();
let _ = require('underscore');

let mongoose = require('mongoose');
let conn = mongoose.createConnection('mongodb://mongo/reception', { useNewUrlParser: true });

conn.on('connected', function() {
    console.log('mongodb connected: reception')
});
conn.on('disconnected', function() {
    console.log('mongodb disconnected')
});
let surveySchema = new mongoose.Schema({
    surveyId: String,
    userId: Number,
    instId: String,
    userKey: String,
    files: [String],
    brFiles: [String],
    psFiles: [String],
    micFiles: [String],
    '101': {t: Number, v: String},
    '102': {t: Number, v: String},
    '201': {t: Number, v: String},
    '202': {t: Number, v: String}
});
let Survey = conn.model('Survey', surveySchema, 'survey');

let amqp = require('amqplib/callback_api');
let amqpConn = null;
amqp.connect('amqp://guest:guest@rabbitmq', function(err, conn) {
    console.log('rabbitmq connected');
    amqpConn = conn;
});

router.post('/survey-answer', function(req, res) {
    let userId = req.headers['user-id'];
    let instId = req.headers['inst-id'];
    let surveyId = req.body.surveyId;
    let entries = req.body.entries;

    Survey.findOne({surveyId: surveyId}, function(err, survey) {
        if (err) return console.error(err);
        if (_.isEmpty(survey)) { // if none
            let baseObj = {
                surveyId: surveyId,
                userId: userId,
                instId: instId
            };

            let obj = Object.assign(baseObj, entries);
            new Survey(obj).save(function(err, s) {
                if (err) return console.error(err);
                console.log('inserted:' + JSON.stringify(s));
            });
        } else { // if exists
            console.log('found: ' + survey);
            let newSurvey = Object.assign(survey, entries);
            newSurvey.save(function(err, s) {
                if (err) return console.error(err);
                console.log('updated:' + JSON.stringify(s));
            });
        }

        res.send({surveyId: surveyId});
    });
});

router.post('/file-metadata', function(req, res) {
    let userId = req.headers['user-id'];
    let instId = req.headers['inst-id'];
    let surveyId = req.body.surveyId;
    let filename = req.body.filename;
    let kind = req.body.kind;

    Survey.findOne({surveyId: surveyId}, function(err, survey) {
        if (err) return console.error(err);
        if (_.isEmpty(survey)) { // if none
            let baseObj = {
                surveyId: surveyId,
                userId: userId,
                instId: instId
            };

            let files = [
                filename
            ];

            let filesObj = {};

            switch(kind) {
                case 'META_MOTION_R':
                    filesObj['brFiles'] = files;
                    break;
                case 'PHONE_SENSOR':
                    filesObj['psFiles'] = files;
                    break;
                case 'PHONE_MIC':
                    filesObj['micFiles'] = files;
                    break;
                default:
                    filesObj['files'] = files;
            }

            let obj = Object.assign(baseObj, filesObj);
            new Survey(obj).save(function(err, s) {
                if (err) return console.error(err);
                console.log('inserted:' + JSON.stringify(s));
            });
        } else { // if exists
            console.log('found: ' + survey);

            let key = null;

            switch(kind) {
                case 'META_MOTION_R':
                    key = 'brFiles';
                    break;
                case 'PHONE_SENSOR':
                    key = 'psFiles';
                    break;
                case 'PHONE_MIC':
                    key = 'micFiles';
                    break;
                default:
                    key = 'files';
            }

            console.log('kind: ' + kind);
            console.log('key: ' + key);

            if (!(key in survey)) {
                survey[key] = [];
            }

            survey[key].push(filename);

            survey.save(function(err, s) {
                if (err) return console.error(err);
                console.log('updated:' + JSON.stringify(s));
            });
        }

        res.send({surveyId: surveyId});
    });
});

router.post('/file-tx-complete', function(req, res) {
    let userId = req.headers['user-id'];
    let instId = req.headers['inst-id'];
    let surveyId = req.body.surveyId;

    Survey.findOne({surveyId: surveyId}, function(err, survey) {
        if (err) return console.error(err);
        if (_.isEmpty(survey)) {

        } else {
            let obj = survey.toObject();
            delete obj['_id'];
            delete obj['__v'];
            console.log('found:' + JSON.stringify(obj));
            let channel = amqpConn.createChannel(function(err, ch) {
                let ex = 'ml.direct';
                let msg = JSON.stringify(obj);

                ch.assertExchange(ex, 'direct', {durable: true});
                ch.publish(ex, '', Buffer.from(msg));
            });

            res.send({surveyId: surveyId});
        }
    });
});

module.exports = router;